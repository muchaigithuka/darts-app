package com.darts;

import com.darts.impl.Darts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DartsAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(DartsAppApplication.class, args);
		Darts darts = new Darts();

		int score = darts.score(1,1);
		System.out.println("Score is " +  score);
	}

}
